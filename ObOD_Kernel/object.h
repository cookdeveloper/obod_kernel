#ifndef OBOD_OBJECT_H
#define OBOD_OBJECT_H

#include "base.h"

namespace ObOD {

class Object {
public:
  Object() = delete;
  Object(const Object& other) = delete;
  Object& operator=(const Object& rhs) = delete;

private:
  ObjectId id_;
  Properties properties_;

  friend class Observer;
  friend class Manager;
};

} // namespace ObOD

#endif // OBOD_OBJECT_H
