#ifndef OBOD_SPACE_H
#define OBOD_SPACE_H

#include "base.h"
#include "object.h"

namespace ObOD {

class Space {
  using ObjectContainer = std::map<ObjectId, Object>;

public:
  Space(SpaceId id);

private:
  SpaceId id_{};
  ObjectContainer objects_;

  friend class Manager;
};

} // namespace ObOD

#endif // OBOD_SPACE_H
