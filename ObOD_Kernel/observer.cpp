#include "observer.h"

namespace ObOD {

Observer::Observer(ObserverId id,
                   ObserverRole role,
                   Authority authority)
  : id_(id),
    role_(role),
    authority_(authority) {

}

} // namespace ObOD

