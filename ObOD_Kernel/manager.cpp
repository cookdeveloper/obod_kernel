#include "manager.h"

#include <functional>

namespace ObOD {

Manager::Manager()
{

}

std::optional<std::reference_wrapper<Space>> Manager::getSpace(SpaceId spaceId)
{
  const auto found_space = spaces_.find(spaceId);
  return (found_space != spaces_.cend()) ? std::optional<std::reference_wrapper<Space>>(found_space->second)
                                         : std::nullopt;
}

std::optional<std::reference_wrapper<Observer>> Manager::getObserver(ObserverId observerId)
{
  const auto found_observer = observers_.find(observerId);
  return (found_observer != observers_.cend()) ? std::optional<std::reference_wrapper<Observer>>(found_observer->second)
                                               : std::nullopt;
}

} // namespace ObOD
