#ifndef OBOD_MANAGER_H
#define OBOD_MANAGER_H

#include <optional>

#include "base.h"
#include "object.h"
#include "observer.h"
#include "space.h"

namespace ObOD {

using Spaces = Map<SpaceId, Space>;
using Observers = Map<ObserverId, Observer>;

class Manager {
public:
  Manager();

private:
  std::optional<std::reference_wrapper<Space>> getSpace(SpaceId spaceId);
  std::optional<std::reference_wrapper<Observer>> getObserver(ObserverId observerId);

private:
  Spaces spaces_;
  Observers observers_;
};

} // namespace ObOD

#endif // OBOD_MANAGER_H
