TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++17

SOURCES += main.cpp \
    object.cpp \
    observer.cpp \
    space.cpp \
    manager.cpp

HEADERS += \
    base.h \
    object.h \
    observer.h \
    space.h \
    manager.h

