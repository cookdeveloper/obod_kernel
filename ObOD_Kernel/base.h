#ifndef OBOD_BASE_H
#define OBOD_BASE_H

#include <any>
#include <map>

namespace ObOD {

// Base types
using String = std::string;
using Real = double;
template <typename K, typename V> using Map = std::map<K, V>;


// Common types
using ID = String;


// Object's types
using ObjectId = ID;

using PropertyId = ID;

using Value = std::any;

using Properties = Map<PropertyId, Value>;


// Observer's types
using ObserverId = ID;

enum class ObserverRole {
  Observer,
  Modifier,
  Creator,
  Destructor
};

using Authority = Real;

using ObservableObjects = Map<ObjectId, Properties>;


// Space's types
using SpaceId = ID;


// Constants
constexpr Authority DEFAULT_AUTHORITY = 0.5;
constexpr ObserverRole DEFAULT_OBSERVER_ROLE = ObserverRole::Observer;

} // namespace ObOD

#endif // OBOD_BASE_H

