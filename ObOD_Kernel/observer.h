#ifndef OBOD_OBSERVER_H
#define OBOD_OBSERVER_H

#include "base.h"
#include "object.h"

namespace ObOD {

class Observer {
public:
  Observer(ObserverId id,
           ObserverRole role = DEFAULT_OBSERVER_ROLE,
           Authority authority = DEFAULT_AUTHORITY);

private:
  ObserverId id_ {};
  ObserverRole role_ {DEFAULT_OBSERVER_ROLE};
  Authority authority_ {DEFAULT_AUTHORITY};
  ObservableObjects observable_list_;

  friend class Manager;
};

} // namespace ObOD

#endif // OBOD_OBSERVER_H
